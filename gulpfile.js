//Declaro las variables

require('es6-promise').polyfill();
var gulp          = require('gulp');
var sass          = require('gulp-sass');
var jade          = require('gulp-jade');
var autoprefixer  = require('gulp-autoprefixer');
var concat        = require('gulp-concat');
var gutil         = require('gulp-util');
var plumber       = require('gulp-plumber');
var imagemin      = require('gulp-imagemin');
var browserSync   = require('browser-sync').create();
var reload        = browserSync.reload;



// Esta tarea me servira para que no se detenga el Wacth, y me muestre el error
var onError = function (err) {
  console.log('¡Hey! alto, hay un ERROR :', gutil.colors.magenta(err.message));
  gutil.beep();
  this.emit('end');
};

// Configuramoas la tarea de javascript
gulp.task('scripts', function() {
    return gulp.src('js/*.js')
    .pipe(plumber({ errorHandler: onError }))
      .pipe(concat('main.js'))
      .pipe(gulp.dest('js/dist/'));
});
// Configuramoas la tarea de jade
gulp.task('jade', function () {
  gulp.src('dev/*.jade')
  .pipe(plumber({ errorHandler: onError }))
  .pipe(jade({pretty:true}))
    .pipe(gulp.dest("./"))
    // .pipe(connect.reload());
});

// Configuramos para preprocesar el Sass
gulp.task('sass', function() {
  return gulp.src('sass/*.scss')
  .pipe(plumber({ errorHandler: onError }))
  .pipe(sass())
  .pipe(autoprefixer())
  .pipe(gulp.dest('./'))
});

gulp.task('images', function() {
  return gulp.src('./images/src/*')

    .pipe(gulp.dest('./images/dist/'));
});

// Configuramos la tarea Wacth, para que se hagan los cambios automaticamente

gulp.task('watch', function() {
  browserSync.init({
    // files: ['./**/*.php'],
    files: ['./*.html'],
    proxy: 'http://localhost/practica',
  });
  gulp.watch('./sass/**/*.scss', ['sass', reload]);
  // gulp.watch('./js/*.js', ['js', reload]);
  gulp.watch('./images/dist/*', ['images', reload]);
  gulp.watch('./js/*.js', ['scripts', reload]);
  gulp.watch('dev/*.jade*', ['jade', reload]);
});


// Solo escribe Gulp en la consola, he inicia todas sus tareas
gulp.task('default', ['sass','images','scripts','jade','watch']);




